package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.jdbc.DataAccessObject;

public class UserDAO {
	private Connection dbConnection;
	private PreparedStatement pStmt;
	
	private String SQL_SELECT = "SELECT name FROM users WHERE email = ? AND password = ?";
	private String SQL_INSERT = "INSERT INTO users (name, email, password, gender) VALUES (?,?,?,?)";
	
	public UserDAO() {
		dbConnection = DataAccessObject.getConnection();
	}
	
	public String verifyUser(String email, String password) {
		String userName = null;
		try {
			pStmt = dbConnection.prepareStatement(SQL_SELECT);
			pStmt.setString(1, email);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				userName = rs.getString("name");
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return userName;
	}
	
	public int createUser(String name, String email, String password, String gender) {
		int result = 0;
		try {
			pStmt = dbConnection.prepareStatement(SQL_INSERT);
			pStmt.setString(1, name);
			pStmt.setString(2, email);
			pStmt.setString(3, password);
			pStmt.setString(4, gender);
			result = pStmt.executeUpdate();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return result;
	}
}
