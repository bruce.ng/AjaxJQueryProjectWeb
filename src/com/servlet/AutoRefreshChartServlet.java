package com.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.jdbc.JDBCPieDataset;

import com.jdbc.DataAccessObject;

/**
 * Servlet implementation class AutoRefreshChartServlet
 */
@WebServlet("/AutoRefreshChartServlet")
public class AutoRefreshChartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection dbConnection = null;
	
	public AutoRefreshChartServlet() {
		dbConnection = DataAccessObject.getConnection();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCPieDataset dataSet = new JDBCPieDataset(dbConnection);
		
		try {
			dataSet.executeQuery("SELECT source, percentage FROM air_pollution ORDER BY percentage");
			JFreeChart chart = ChartFactory.createPieChart("Source of Air Pollution ", // Title
					dataSet, // Data
					true, // Display the legend
					true, // Display tool tips
					false // No URLs
					);
			
			if (chart != null) {
				chart.setBorderVisible(true);
				int width = 600;
				int height = 400;
				response.setContentType("image/jpeg");
				OutputStream out = response.getOutputStream();
				ChartUtilities.writeChartAsJPEG(out, chart, width, height);
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
}
