package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class DataAccessObject {
	
	private static Connection	connection	= null;
	
	public static Connection getConnection() {
		if (connection != null) {
			System.out.println(connection);
			return connection;
		} else {
			// Store the database URL in a string
			String serverName = "127.0.0.1";
			String portNumber = "3306";
			String sid = "proajax";
			String dbUrl = "jdbc:mysql://" + serverName + ":" + portNumber + "/" + sid + "?profileSQL=true";
			try {
				Class.forName("com.mysql.jdbc.Driver"); 
				// set the url, username and password for the database
				connection = DriverManager.getConnection(dbUrl, "root", "0000");
				/**
				 * user && pass use system ubuntu
				 * */
//				connection = DriverManager.getConnection(dbUrl);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println(connection);
			return connection;
		}
	}
}
