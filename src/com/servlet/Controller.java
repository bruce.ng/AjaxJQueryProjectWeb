package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CrudDao;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.model.Student;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	
	private static final long	serialVersionUID	= 1L;
	private CrudDao						dao								= new CrudDao();
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		System.out.println("====action====" + action);
		
		if (action != null) {
			
			List<Student> studentList = new ArrayList<Student>();
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			response.setContentType("application/json");
			
			/*				if (action.equals("list")) {
								try {
									// Add data to Student list
									studentList.add(new Student(1, "Haripriya", "IT", "xyz@xyz.com"));
									studentList.add(new Student(2, "Dinesh", "ECE", "xyz@gmail.com"));
									studentList.add(new Student(3, "Prabhu", "MECH", "abc@gmail.com"));
									studentList.add(new Student(4, "Badru", "ECE", "efg@gmail.com"));
									studentList.add(new Student(5, "Lahir nisha", "CSC", "xyz@gmail.com"));
									studentList.add(new Student(6, "Nilafar nisha", "CSC", "123@gmail.com"));
									studentList.add(new Student(7, "Jamil", "ECE", "789@gmail.com"));
									studentList.add(new Student(8, "Mahes", "ECE", "123@gmail.com"));
									studentList.add(new Student(9, "Lourde", "IT", "xyz@gmail.com"));
									
									// Convert Java Object to Json
									String jsonArray = gson.toJson(studentList);
									
									//Return Json in the format required by jTable plugin
									jsonArray = "{\"Result\":\"OK\",\"Records\":" + jsonArray + "}";
									//System.out.println(jsonArray);
									response.getWriter().print(jsonArray);
								} catch (Exception ex) {
									String error = "{\"Result\":\"ERROR\",\"Message\":" + ex.getMessage() + "}";
									response.getWriter().print(error);
								}
							}*/
			
			/*			if (action.equals("list")) {
							try {
								// Fetch Data from Student Table
								studentList = dao.getAllStudents();
								// Convert Java Object to Json
								String jsonArray = gson.toJson(studentList);
								
								// Return Json in the format required by jTable plugin
								jsonArray = "{\"Result\":\"OK\",\"Records\":" + jsonArray + "}";
								response.getWriter().print(jsonArray);
							} catch (Exception e) {
								String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getMessage() + "\"}";
								response.getWriter().print(error);
								e.printStackTrace();
							}
						}
			*/
			if (action.equals("list")) {
				try {
					// Fetch Data from User Table
					int startPageIndex = Integer.parseInt(request.getParameter("jtStartIndex"));
					int recordsPerPage = Integer.parseInt(request.getParameter("jtPageSize"));
					
					// Fetch Data from Student Table
					studentList = dao.getAllStudents(startPageIndex, recordsPerPage);
					// Get Total Record Count for Pagination
					int userCount = dao.getStudentCount();
					// Convert Java Object to Json
					String jsonArray = gson.toJson(studentList);
					
					// Return Json in the format required by jTable plugin
					jsonArray = "{\"Result\":\"OK\",\"Records\":" + jsonArray + ",\"TotalRecordCount\":" + userCount + "}";
					response.getWriter().print(jsonArray);
				} catch (Exception ex) {
					String error = "{\"Result\":\"ERROR\",\"Message\":" + ex.getMessage() + "}";
					response.getWriter().print(error);
				}
			}
			
			if (action.equals("create")) {
				Student student = new Student();
				
				if (request.getParameter("studentId") != null) {
					int studentId = Integer.parseInt(request.getParameter("studentId"));
					student.setStudentId(studentId);
				}
				
				if (request.getParameter("studentName") != null) {
					String name = request.getParameter("studentName");
					student.setStudentName(name);
				}
				
				if (request.getParameter("department") != null) {
					String department = request.getParameter("department");
					student.setDepartment(department);
				}
				
				if (request.getParameter("emailId") != null) {
					String emailId = request.getParameter("emailId");
					student.setEmailId(emailId);
				}
				
				try {
					// Create new record
					dao.addStudent(student);
					// Convert Java Object to Json
					String json = gson.toJson(student);
					// Return Json in the format required by jTable plugin
					String jsonData = "{\"Result\":\"OK\",\"Record\":" + json + "}";
					response.getWriter().print(jsonData);
				} catch (Exception e) {
					String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getMessage() + "\"}";
					response.getWriter().print(error);
					e.printStackTrace();
				}
			}
			
			if (action.equals("update")) {
				Student student = new Student();
				if (request.getParameter("studentId") != null) {
					int studentId = Integer.parseInt(request.getParameter("studentId"));
					student.setStudentId(studentId);
				}
				
				if (request.getParameter("name") != null) {
					String name = request.getParameter("name");
					student.setStudentName(name);
				}
				
				if (request.getParameter("department") != null) {
					String department = request.getParameter("department");
					student.setDepartment(department);
				}
				
				if (request.getParameter("emailId") != null) {
					String emailId = request.getParameter("emailId");
					student.setEmailId(emailId);
				}
				
				try {
					// Update existing record
					dao.updateStudent(student);
					// Convert Java Object to Json
					String json = gson.toJson(student);
					String jsonData = "{\"Result\":\"OK\",\"Record\":" + json + "}";
					response.getWriter().print(jsonData);
				} catch (Exception e) {
					String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getMessage() + "\"}";
					response.getWriter().print(error);
					e.printStackTrace();
				}
			}
			
			if (action.equals("delete")) {
				try {
					// Delete record
					if (request.getParameter("studentId") != null) {
						int studentId = Integer.parseInt(request.getParameter("studentId"));
						dao.deleteStudent(studentId);
						String jsonData = "{\"Result\":\"OK\"}";
						response.getWriter().print(jsonData);
					}
				} catch (Exception e) {
					String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getMessage() + "\"}";
					response.getWriter().print(error);
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * n case of read operations in jTable, Result property must be “OK” if operation is successful. If an error occurs, then Result property must be “ERROR”. If Result is “OK”, the Records property will contain an array of records defined in the servlet. If it’s ERROR, a Message property will contain an error message to show to the user. A sample return value for listAction is show below
	 
	{“Result”:”OK”,”Records”:[
	{
	“studentId”: 2,
	“name”: “Bashit”,
	“department”: “EEE”,
	“emailId”: “xyz@abc.com”
	},
	{
	“studentId”: 3,
	“name”: “Haripriya”,
	“department”: “IT”,
	“emailId”: “hp@abc.com”
	}
	]}
	 * */
}
