package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jdbc.DataAccessObject;
import com.model.Student;

public class CrudDao {
	
	private Connection				dbConnection;
	private PreparedStatement	pStmt;
	
	public CrudDao() {
		dbConnection = DataAccessObject.getConnection();
	}
	
	public void addStudent(Student student) {
		String insertQuery = "INSERT INTO Student(studentid, studentname, department, email) VALUES (?,?,?,?)";
		try {
			pStmt = DataAccessObject.getConnection().prepareStatement(insertQuery);
			pStmt.setInt(1, student.getStudentId());
			pStmt.setString(2, student.getStudentName());
			pStmt.setString(3, student.getDepartment());
			pStmt.setString(4, student.getEmailId());
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteStudent(int userId) {
		String deleteQuery = "DELETE FROM Student WHERE studentid = ?";
		try {
			pStmt = DataAccessObject.getConnection().prepareStatement(deleteQuery);
			pStmt.setInt(1, userId);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateStudent(Student student) {
		String updateQuery = "UPDATE Student SET studentname = ?, " + "department = ?, email = ? WHERE studentid = ?";
		try {
			pStmt = DataAccessObject.getConnection().prepareStatement(updateQuery);
			pStmt.setString(1, student.getStudentName());
			pStmt.setString(2, student.getDepartment());
			pStmt.setString(3, student.getEmailId());
			pStmt.setInt(4, student.getStudentId());
			pStmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method old
	 * */
	/*	public List<Student> getAllStudents() {
			List<Student> students = new ArrayList<Student>();
			
			String query = "SELECT * FROM Student ORDER BY studentid";
			try {
				Statement stmt = DataAccessObject.getConnection().createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					Student student = new Student();
					
					student.setStudentId(rs.getInt("studentid"));
					student.setStudentName(rs.getString("studentname"));
					student.setDepartment(rs.getString("department"));
					student.setEmailId(rs.getString("email"));
					students.add(student);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return students;
		}*/
	
	/**
	 * Method new
	 * */
	public List<Student> getAllStudents(int startPageIndex, int recordsPerPage) {
		List<Student> students = new ArrayList<Student>();
		int range = startPageIndex + recordsPerPage;
		//Oracle
		//String query = "SELECT * from (Select M.*, Rownum R From student M) where r > " + startPageIndex + " and r <= " + range;
		//Mysql
		String query = "select * from Student limit " + startPageIndex + "," + range;
		System.out.println(query);
		try {
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				Student student = new Student();
				
				student.setStudentId(rs.getInt("studentid"));
				student.setStudentName(rs.getString("studentname"));
				student.setDepartment(rs.getString("department"));
				student.setEmailId(rs.getString("email"));
				students.add(student);
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return students;
	}
	
	public int getStudentCount() {
		int count = 0;
		try {
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS COUNT FROM Student");
			while (rs.next()) {
				count = rs.getInt("COUNT");
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return count;
	}
	
}
