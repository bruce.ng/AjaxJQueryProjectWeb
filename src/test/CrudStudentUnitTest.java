package test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jdbc.DataAccessObject;
import com.model.Student;

public class CrudStudentUnitTest {
	
	@Test
	public void testgetAllStudents() {
		List<Student> students = new ArrayList<Student>();
		
		String query = "SELECT * FROM Student ORDER BY studentid";
		try {
			Statement stmt = DataAccessObject.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				Student student = new Student();
				
				student.setStudentId(rs.getInt("studentid"));
				student.setStudentName(rs.getString("studentname"));
				student.setDepartment(rs.getString("department"));
				student.setEmailId(rs.getString("email"));
				students.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.print(students);
	}
}
