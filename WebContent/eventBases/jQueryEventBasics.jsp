<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jQuery Event Basics</title>
<script src="../js/jquery/jquery-1.11.2.min.js" type="text/javascript"></script>
</head>
<body>
	<p>nguyendinhminh</p>
	<button class="alert">Button First</button>
	<input type="text" id="name" name="name">
	<a href="/evil/">click tag a here</a>
	
	<p id="runOnlyOne">Events to Run Only Once</p>
	
	<input type="text" id="multiOnlyOnce" name="multiOnlyOnce">
</body>
<style>
.evil {
	background-color: red;
}
</style>

<script type="text/javascript">
<!--
	//-->

$(document).ready(function() {
		
		// Sets up click behavior on all button elements with the alert class
		// that exist in the DOM when the instruction was executed
		$("button.alert").on("click", function() {
			console.log("A button with the alert class was clicked!");
		});
		
		// Now create a new button element with the alert class. This button
		// was created after the click listeners were applied above, so it
		// will not have the same click behavior as its peers
		$("<button class='alert'>Alert!</button>").appendTo(document.body);
		
		//Equivalent event setup using the `.on()` method
		$("p").on("click", function() {
			console.log("click");
		});
		
		
		// Event setup using the `.on()` method with data
		$("input").on("change", {
				foo : 'minhnd'
			}, // Associate data with event binding
			function(eventObject) {
				console.log("An input value has changed! ", eventObject.data.foo);
		});
		
		
		// Preventing a link from being followed
		$( "a" ).click(function( eventObject ) {
			var elem = $(this);
			if (elem.attr("href").match(/evil/)) {
				eventObject.preventDefault();
				elem.addClass("evil");
				console.log('click tag a !!!');
			}
		});
		
		// Multiple events, same handler
		$( "input" ).on(
			"click change", // Bind handlers for multiple events
			function() {
				console.log( "An input was clicked or changed!" );
			}
		);
		
		
		// Binding multiple events with different handlers
		$("p").on({
			"click": function() { console.log( "clicked!" ); },
			"mouseover": function() { console.log( "hovered!" ); }
		});
		
		// Switching handlers using the `.one()` method
		
		$("#runOnlyOne").one("click", firstClick);
		function firstClick() {
			console.log("You just clicked this for the first time!");
			// Now set up the new handler for subsequent clicks;
			// omit this step if no further click responses are needed
			$(this).click(function() {
				console.log("You have clicked this before!");
			});
		}
		
		// Using .one() to bind several events
		$( "input[id='multiOnlyOnce']" ).one( "focus mouseover keydown", firstEvent);
		function firstEvent( eventObject ) {
			console.log( "A " + eventObject.type + " event occurred for the first time on the input with id " + this.id );
		}
});
</script>
</html>
