<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Gridview</title>
<script src="js/jquery/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.min.js" type="text/javascript"></script>

<link href="css/jquery-ui.min.css" rel="stylesheet" />

<link href="css/grid_table_jui.css" rel="stylesheet" />
<link href="css/grid_page.css" rel="stylesheet" />

<script>
	// Ajax call to Servlet to display data via DataTables
    $(document).ready(function() {
	    $(".jqueryDataTable").dataTable({
	        "sPaginationType" : "full_numbers",
	        "bProcessing" : false,
	        "bServerSide" : false,
	        "sAjaxSource" : "displayData",
	        "bJQueryUI" : true,
	        "aoColumns" : [ {
		        "mData" : "company"
	        }, {
		        "mData" : "country"
	        }, {
		        "mData" : "year"
	        }, {
		        "mData" : "revenue"
	        } ]
	    });
    });
</script>
</head>
<body id="dt_example">
	<div id="container">
		<h1>Ajax based Gridview using jQuery DataTable plugin</h1>
		<div id="demo_jui">
			<table class="display jqueryDataTable">
				<thead>
					<tr>
						<th>Company</th>
						<th>Country</th>
						<th>Year</th>
						<th>Revenue</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>