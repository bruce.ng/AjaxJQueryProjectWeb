<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>CRUD operations using jTable in J2EE</title>
	<!-- jTable Metro styles. -->
	<link href="css/jtable/metro/blue/jtable.min.css" rel="stylesheet" type="text/css" />
	<link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	
	<!-- jTable script file. -->
	<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
	<script src="js/jquery.jtable.min.js" type="text/javascript"></script>
	
	<!-- User Defined Jtable js file -->
	<script src="js/userDefieneJTable.js" type="text/javascript"></script>
</head>

<body>
	<div style="text-align: center;">
		<h4>AJAX based CRUD operations using jTable in J2ee</h4>
		<div id="StudentTableContainer"></div>
	</div>
</body>
</html>

<!--
Totorial

Changes from the server’s perspective: Servlet
 
If paging is enabled in jsp then jTable sends two query string parameters to the server on listAction AJAX call:
•	jtStartIndex: Start index of records for current page.
•	jtPageSize: Count of maximum expected records.
And it expects additional information from server:
•	TotalRecordCount: Total count of records. 
 -->