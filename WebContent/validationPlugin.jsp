<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jQuery form validation</title>
<!-- Include jQuery form & jQuery script file. -->
<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/userDefieneValidation.js" type="text/javascript"></script>

<!-- Include css styles here -->
<link href="css/styleValidation.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<h3>jQuery form validation using jQuery Validation plugin</h3>
	<form id="form" name="" action="">
		<div class="fieldgroup">
			<label for="name">Name:</label> <input name="name" size="20" />
			<p>
				<input class="submit" type="submit" value="Submit" />
			</p>
		</div>
	</form>
</body>
</html>