package com.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.jdbc.DataAccessObject;

public class DataDao {
	
	public ArrayList<String> getFrameWork(String frameWork) {
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement ps = null;
		String data;
		try {
			ps = DataAccessObject.getConnection().prepareStatement("select * FROM dm_ship_agency where ShipAgencyNameVN LIKE ?");
			ps.setString(1, "%" + frameWork + "%");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				data = rs.getString("ShipAgencyNameVN");
				list.add(data);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return list;
	}
}
