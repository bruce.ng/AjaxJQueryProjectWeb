package com.model;

public class Student {
	public Student() {
	}
	
	public Student(int studentId, String studentName, String department, String emailId) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.department = department;
		this.emailId = emailId;
	}
	
	private int			studentId;
	private String	studentName;
	private String	department;
	private String	emailId;
	
	public int getStudentId() {
		return studentId;
	}
	
	public String getStudentName() {
		return studentName;
	}
	
	public String getDepartment() {
		return department;
	}
	
	public String getEmailId() {
		return emailId;
	}
	
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	
	public void setDepartment(String department) {
		this.department = department;
	}
	
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
}
